package com.iclaim.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends Activity {
    public static final String Stored_Token = "TokenFile";
    EditText txtSsn;
    EditText txtPIN;

    SharedPreferences appPref;
    SharedPreferences.Editor appPrefEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        // Set up the login form.
        txtSsn = (EditText) findViewById(R.id.txtSSN);
        txtPIN = (EditText) findViewById(R.id.txtPIN);
    }

    public void loginClick(View view) {

        Intent c = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(c);


        // Validate the log in data
        boolean validationError = false;
        StringBuilder validationErrorMessage =
                new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(txtSsn )) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_ssn));
        }
        if (isEmpty(txtPIN)) {
            if (validationError) {
                validationErrorMessage.append(getResources().getString(R.string.error_join));
            }
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_pin));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));
        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(LoginActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                    .show();
            return;
        }

        /* commented out until we have the test server up and running
        MessageDigest encoded = null;
        try {
            encoded = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String text = txtPIN.getText().toString();
        try {
            encoded.update(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.v("ewwor", "someting wong");
            e.printStackTrace();
        }
        byte[] passhash = encoded.digest();
        String hshed2 = passhash.toString();
        Log.i("password", hshed2);
        BigInteger bigInt = new BigInteger(1, passhash);
        String hashed = bigInt.toString(16);
        Log.i("password", hashed);
        String txtSSN = txtSsn.getText().toString();
        String token = (txtSSN + ":" + hashed);

        byte[] data = new byte[0];
        try {
            data = token.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String encodedhash = Base64.encodeToString(data, Base64.NO_WRAP);
        final String basicAuth = "Basic " + encodedhash;
        try {
            Resources resources = getResources();
            final String urla = String.format(resources.getString(R.string.web_page_a));

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(urla);
            httpget.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpget.setHeader("Authorization", basicAuth);
            HttpResponse response = client.execute(httpget);
            int statusCode = response.getStatusLine().getStatusCode();
            Header[] headers = response.getAllHeaders();
            for (Header header : headers) {
                System.out.println("Key : " + header.getName()
                        + " ,Value : " + header.getValue());
            }
            HttpEntity resEntityGet = response.getEntity();
            if (resEntityGet != null) {
                if (statusCode != HttpStatus.SC_OK) {


                    Toast toast = Toast.makeText(this, "  Invalid Credentials  \n  Access Denied!   ", Toast.LENGTH_SHORT);

                    toast.show();

                    Log.i("TAG", "Error in web request: " + statusCode + " - " + response.getFirstHeader("DEW_MSG").getValue());

                } else {
                    //do something with the response
                    String DEW_MSG = response.getFirstHeader("DEW_MSG").getValue();
                    String DEW_TokenKey = response.getFirstHeader("DEW_TokenKey").getValue();
                    if (DEW_MSG != null && DEW_TokenKey != null) {
                        //Log.i("DEW", DEW_TokenKey);
                        //Log.i("DEW_MSG", DEW_MSG);

                        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
                        editor.putString("Token", DEW_TokenKey);
                        editor.apply();

                        Intent i = new Intent(this, GetQuestionsService.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        this.startService(i);


                        Intent c = new Intent(LoginActivity.this, MainFragment.class);
                        startActivity(c);

                        finish();
                    }
                }


            }
        }catch(ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
      }   */
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
