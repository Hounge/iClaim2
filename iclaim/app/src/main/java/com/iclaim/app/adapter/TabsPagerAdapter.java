package com.iclaim.app.adapter;

import com.iclaim.app.MainFragment;
import com.iclaim.app.FAQFragment;
import com.iclaim.app.LocateFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new MainFragment();
            case 1:
                // Games fragment activity
                return new LocateFragment();
            case 2:
                // Movies fragment activity
                return new FAQFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }

}
