package com.iclaim.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Created by hounge on 11/21/14.
 */
public class LogActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_activity);
    }
    public void submitClick(View view) {
        Toast.makeText(getApplicationContext(), "Filing Claim Please wait",
                Toast.LENGTH_LONG).show();


    }
    public void cancelClick(View view) {
        Toast.makeText(getApplicationContext(), "Claim Cancelled please file your claim",
                Toast.LENGTH_LONG).show();

    }
}
