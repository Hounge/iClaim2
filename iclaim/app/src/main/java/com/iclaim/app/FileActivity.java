package com.iclaim.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Created by hounge on 11/21/14.
 */
public class FileActivity extends Activity{
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.questions_activity);
        }
    public void negativeClick(View view) {
        Toast.makeText(getApplicationContext(), "you clicked No",
                Toast.LENGTH_LONG).show();


    }
    public void positiveClick(View view) {
        Toast.makeText(getApplicationContext(), "you clicked Yes",
                Toast.LENGTH_LONG).show();

    }

    public void logoutClick(View view) {

        Intent c = new Intent(FileActivity.this, LoginActivity.class);
        startActivity(c);
    }
}
